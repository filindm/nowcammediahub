# coding=UTF-8

import os
import traceback
from flask import Flask, request, render_template, jsonify, redirect, url_for
from flask import make_response, abort
from werkzeug import Response
from flask.json import JSONEncoder
from bson.objectid import ObjectId
from pymongo import MongoClient, DESCENDING
import gridfs
import base64
from io import BytesIO
import mimetypes
import requests
import printing
import camera
import logging
import logging.config
import functools
import timeit
import arrow
from PIL import Image, ImageOps
import random
import uuid
import cors


app = Flask(__name__)
app.config.from_object('nowcammediahub.config')
if 'NOWCAM_CENTRAL_URL' in os.environ:
    app.config.update(NOWCAM_CENTRAL_URL=os.environ['NOWCAM_CENTRAL_URL'])
app.config.update({x[len("NOWCAM_"):]:os.environ[x] for x in os.environ if x.startswith("NOWCAM_")})

VERIFY_SSL=False
AUTH_HDRS = {'Authorization': 'JWT {}'.format(app.config['TOKEN'])}
print AUTH_HDRS


class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, ObjectId):
            return str(obj)
        return JSONEncoder.default(self, obj)

app.json_encoder = CustomJSONEncoder


db = MongoClient(host=app.config["MONGO_URL"], connect=False)[app.config["MONGO_DBNAME"]]
fs = gridfs.GridFS(db)
db.fs.files.create_index('code')

logging.config.dictConfig(app.config['LOGGING'])
logger = logging.getLogger(__name__)


def run():
    from cherrypy import wsgiserver
    d = wsgiserver.WSGIPathInfoDispatcher({'/': app})
    server = wsgiserver.CherryPyWSGIServer(('0.0.0.0', 5010), d)
    try:
        server.start()
    except KeyboardInterrupt:
        server.stop()


def run_debug():
    print '================================================='
    print '===         Starting in DEBUG mode            ==='
    print '=== NOWCAM_CENTRAL_URL: {} ==='.format(app.config['NOWCAM_CENTRAL_URL'])
    print '================================================='
    app.run(host='0.0.0.0', port=5010, debug=True)


from broadcast import main as broadcast_main
from tethered_camera import main as tethered_camera_main
from video_gif import main as video_gif_main


def get_photo_printer():
    try:
        return db.settings.find_one({'_id': 1}).get('photo_printer')
    except:
        return ''


def get_ticket_printer():
    try:
        return db.settings.find_one({'_id': 1}).get('ticket_printer')
    except:
        return ''


def get_default_event():
    try:
        return db.settings.find_one({'_id': 1}).get('default_event')
    except:
        return ''


def enum_events():
    try:
        res = requests.get(app.config['NOWCAM_CENTRAL_URL'] + '/events', 
            headers=AUTH_HDRS, verify=VERIFY_SSL)
        res.raise_for_status()
        res = res.json()
        return res['items']
    except:
        traceback.print_exc()
        return []


def update_image_prop(img_id, prop_name, prop_value):
    db.fs.files.update_one({'_id': img_id}, {'$set': {prop_name: prop_value}})


def update_image_stat(uuid, stat):
    try:
        requests.post(app.config['NOWCAM_CENTRAL_URL'] + '/events/' + uuid + '/stat/' + stat,
            headers=AUTH_HDRS,
            verify=VERIFY_SSL).raise_for_status()
    except Exception as ex:
        logger.warn(ex)


def benchmark(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        logger.debug('>>> {}'.format(func.func_name))
        start_time = timeit.default_timer()
        res = func(*args, **kwargs)
        elapsed = timeit.default_timer() - start_time
        logger.debug('<<< {}: {}'.format(func.func_name, elapsed))
        return res
    return wrapper


def print_img(id):
    try:
        img = fs.get(id)
        update_image_prop(id, 'print_status', 'PENDING')
        logger.debug('print_img: {}, id: {}'.format(img.name, id))
        printing.print_image(img.name, img, get_photo_printer())
        update_image_prop(id, 'print_status', 'COMPLETED')
        if img.uuid is not None:
            update_image_stat(img.uuid, 'printed')
    except Exception as exc:
        update_image_prop(id, 'print_status', 'FAILED')
        logger.error(exc)


def print_tkt(id):
    try:
        ticket_image_id = get_default_event()['ticket_image_id']
        ticket_img = fs.get(ticket_image_id)
        img = fs.get(id)
        text_box = {
            'top': get_default_event().get('ticket_top', 0),
            'left' : get_default_event().get('ticket_left', 0),
            'width': get_default_event().get('ticket_width', 100),
            'height': get_default_event().get('ticket_height', 100)
        }
        update_image_prop(id, 'print_ticket_status', 'PENDING')
        logger.debug('print_ticket: {}, id: {}'.format(img.name, id))
        printing.print_ticket(img.code, img, ticket_img, get_ticket_printer(), text_box)
        update_image_prop(id, 'print_ticket_status', 'COMPLETED')
    except Exception as exc:
        update_image_prop(id, 'print_ticket_status', 'FAILED')
        logger.error(exc)


def upload_img(img_id):
    img = fs.get(img_id)
    update_image_prop(img_id, 'upload_status', 'PENDING')
    logger.debug('upload_img: {}, id: {}, event_id: {}'.format(img.name, img_id, img.event_id))
    try:
        requests.post(app.config['NOWCAM_CENTRAL_URL'] + '/photos', 
            files={'file': img}, 
            data={
                'name': img.name, 
                'event': img.event_id, 
                'unit': img.unit_id, 
                'code': img.code,
                'uuid': img.uuid
            },
            headers=AUTH_HDRS,
            verify=VERIFY_SSL).raise_for_status()
        update_image_prop(img_id, 'upload_status', 'COMPLETED')
    except Exception as exc:
        update_image_prop(img_id, 'upload_status', 'FAILED')


def start_edit_img(img_id):
    img = fs.get(img_id)
    update_image_prop(img_id, 'edit_status', 'PENDING')


@app.route('/photos/<id>/finish_edit', methods=['POST'])
def finish_edit_img(self, id, discard_img=False):
    discard_img = bool(request.args.get('discard_img', False))
    if discard_img:
        fs.delete(id)
    else:
        img = fs.get(id)
        update_image_prop(id, 'edit_status', 'COMPLETED')
        print_img(id)
        print_tkt(id)
        upload_img(id)
    return 'ok\n'


@app.route('/')
# @benchmark
def index():
    return app.send_static_file('index.html')


@app.route('/edit.html')
# @benchmark
def edit():
    return app.send_static_file('edit.html')


# @app.route('/rfid.html')
# # @benchmark
# def rfid():
#     return app.send_static_file('rfid.html')

import nowcammediahub.rfid


### Photos

@app.route('/photos', methods=['GET'])
# @benchmark
def list_photos():
    skip = int(request.args.get('skip', 0))
    limit = int(request.args.get('limit', 25))
    code = request.args.get('code')
    edit_only = bool(request.args.get('edit_only', False))
    event_id = get_default_event()['_id']
    not_thumbnail_filter = {'is_thumbnail': {'$ne': True}}
    event_filter = {'event_id': event_id}
    fltr = {'$and': [event_filter, not_thumbnail_filter]}
    if code:
        fltr['$and'].append({'code': code})
    if edit_only:
        fltr['$and'].append({'edit_status': 'PENDING'})
    photos = db.fs.files.find(fltr, skip=skip, limit=limit).sort('uploadDate', DESCENDING)
    upload_status_count = db.fs.files.aggregate([{
        '$match': fltr
    },{
        '$group': {'_id': '$upload_status', 'count': {'$sum': 1}}
    }])
    print_status_count = db.fs.files.aggregate([{
        '$match': fltr
    },{
        '$group': {'_id': '$print_status', 'count': {'$sum': 1}}
    }])
    print_ticket_status_count = db.fs.files.aggregate([{
        '$match': fltr
    },{
        '$group': {'_id': '$print_ticket_status', 'count': {'$sum': 1}}
    }])
    return jsonify({
        'items': [p for p in photos],
        'total': db.fs.files.count(fltr),
        'upload_status_count': {x['_id']: x['count'] for x in upload_status_count},
        'print_status_count': {x['_id']: x['count'] for x in print_status_count},
        'print_ticket_status_count': {x['_id']: x['count'] for x in print_ticket_status_count},
    })


PHOTO_CODE_ALPHABET_1 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
PHOTO_CODE_ALPHABET_2 = '01234567890'
PHOTO_THUMBNAIL_SIZE = (242, 200)

'''
This operation on mongodb is vulnerable to race conditions, but all requests with the same
event_id and group are serialized in tethered_camera.py, so we're good here.
'''
def gen_photo_code(event_id, group):
    if group:
        res = db.photo_groups.find_one({'event_id': event_id, 'group': group})
        if res and 'code' in res:
            return res['code']
    chars = []
    for i in range(4):
        chars.append(random.choice(PHOTO_CODE_ALPHABET_1))
    for i in range(4):
        chars.append(random.choice(PHOTO_CODE_ALPHABET_2))
    code = ''.join(chars)
    if group:
        db.photo_groups.insert_one({'event_id': event_id, 'group': group, 'code': code})
    return code


def _get_last_photo(event_id, unit_id):
    logger.debug('_get_last_photo: event_id: {}, unit_id: {}'.format(event_id, unit_id))
    return db.fs.files.find_one(
        {'event_id': event_id, 'unit_id': unit_id}, 
        sort=[('import_key', -1)])


def upload_photo_from_photobooth():
    incoming_photo = request.files['file']
    incoming_photo_filename = incoming_photo.filename
    incoming_photo_mimetype = 'image/jpeg' # we get image/png and later convert it to image/jpeg
    rfid = request.form['rfid']
    event_id = get_default_event()['_id']

    logger.debug('upload_photo: {}, event_id: {}, rfid: {}'.format(
        incoming_photo_filename, event_id, rfid))
    
    logger.debug('receiving photo: {}'.format(incoming_photo_filename))
    content_type = incoming_photo_mimetype or mimetypes.guess_type(incoming_photo_filename)[0]
        
    # Add overlay
    if not incoming_photo_filename[-4:] == '.gif':
        try:
            overlay = fs.get(get_default_event()['overlay'])
            img = Image.open(incoming_photo).convert('RGBA')
            overlay = Image.open(overlay).convert('RGBA').resize(img.size)
            img = Image.alpha_composite(img, overlay)
            buf = BytesIO()
            img.save(buf, 'JPEG') # !!!
            buf.seek(0)
            incoming_photo = buf
        except Exception as ex:
            logger.warn(ex)

    thumb = BytesIO()
    if content_type == 'image/jpeg':
        ImageOps.fit(Image.open(incoming_photo), PHOTO_THUMBNAIL_SIZE, Image.ANTIALIAS).save(thumb, 'JPEG')
    elif content_type == 'image/gif':
        ImageOps.fit(Image.open(incoming_photo), PHOTO_THUMBNAIL_SIZE, Image.ANTIALIAS).save(thumb, 'GIF')
    else:
        abort(400, 'Unsupported content type: ' + content_type)
    thumb.seek(0)
    thumb_id = fs.put(thumb, 
        filename='thumb_' + incoming_photo_filename,
        contentType=content_type,
        is_thumbnail=True
    )
    incoming_photo.seek(0)
    id = fs.put(incoming_photo, 
        filename=incoming_photo_filename, 
        contentType=content_type,        # title=title, 
        unit_id='photobooth',
        event_id=event_id,
        # import_key=import_key,
        thumb_id=thumb_id,
        code=gen_photo_code(event_id, None),
        uuid=uuid.uuid4().hex
    )
    # start_edit_img(id)
    upload_img(id)
    print_img(id)
    print_tkt(id)
    return 'ok\n'


@app.route('/photos', methods=['POST'])
# @benchmark
@cors.crossdomain(origin='*')
def upload_photo():
    if request.form.get('rfid'):
        return upload_photo_from_photobooth()
    incoming_photo = request.files['file']
    incoming_photo_filename = incoming_photo.filename
    incoming_photo_mimetype = incoming_photo.mimetype
    unit_id = request.form['unit_id']
    event_id = get_default_event()['_id']

    logger.debug('upload_photo: {}, unit_id: {}, event_id: {}'.format(
        incoming_photo_filename, unit_id, event_id))
    
    # TODO: check unit is assigned to current event
    
    import_key = request.form['key']
    group = request.form.get('group')

    last_photo = _get_last_photo(event_id, unit_id)
    if last_photo is not None:
        last_key = last_photo['import_key']
    else:
        last_key = ' '

    if last_key < import_key:
        logger.debug('receiving photo: {}'.format(incoming_photo_filename))
        content_type = incoming_photo_mimetype or mimetypes.guess_type(incoming_photo_filename)[0]
        
        # Add overlay
        if not incoming_photo_filename[-4:] == '.gif':
            try:
                overlay = fs.get(get_default_event()['overlay'])
                img = Image.open(incoming_photo).convert('RGBA')
                overlay = Image.open(overlay).convert('RGBA').resize(img.size)
                img = Image.alpha_composite(img, overlay)
                buf = BytesIO()
                img.save(buf, 'JPEG') # !!!
                buf.seek(0)
                incoming_photo = buf
            except Exception as ex:
                logger.warn(ex)

        thumb = BytesIO()
        if content_type == 'image/jpeg':
            ImageOps.fit(Image.open(incoming_photo), PHOTO_THUMBNAIL_SIZE, Image.ANTIALIAS).save(thumb, 'JPEG')
        elif content_type == 'image/gif':
            ImageOps.fit(Image.open(incoming_photo), PHOTO_THUMBNAIL_SIZE, Image.ANTIALIAS).save(thumb, 'GIF')
        else:
            abort(400, 'Unsupported content type: ' + content_type)
        thumb.seek(0)
        thumb_id = fs.put(thumb, 
            filename='thumb_' + incoming_photo_filename,
            contentType=content_type,
            is_thumbnail=True
        )
        incoming_photo.seek(0)
        id = fs.put(incoming_photo, 
            filename=incoming_photo_filename, 
            contentType=content_type,
            # title=title, 
            unit_id=unit_id,
            event_id=event_id,
            import_key=import_key,
            thumb_id=thumb_id,
            code=gen_photo_code(event_id, group),
            uuid=uuid.uuid4().hex
        )
        start_edit_img(id)
        upload_img(id)
        print_img(id)
        if group:
            res = db.photo_groups.find_one({'event_id': event_id, 'group': group, 'ticket_printed': True})
            if not res:
                print_tkt(id)
                db.photo_groups.update_one({'event_id': event_id, 'group': group}, {
                    '$set': {'event_id': event_id, 'group': group, 'ticket_printed': True}}, upsert=True)
        else:
            print_tkt(id)
        return 'ok\n'
    else:
        logger.warn('duplicate sent: {}'.format(import_key))
        return 'duplicate\n'


@app.route('/photos/edited', methods=['POST'])
# @benchmark
def upload_edited_photo():
    data = request.json['data']
    unit_id = '__user_edited__'
    event_id = get_default_event()['_id']

    logger.debug('upload_photo: {}, unit_id: {}, event_id: {}'.format(
        'user edited photo', unit_id, event_id))

    data = data.split(';base64,', 2)
    content_type = data[0].split(':')[1]
    data = data[1].decode('base64')

    if content_type is None or len(content_type) == 0:
        logger.warn('content_type is None or len(content_type) == 0')
        abort(400)

    if data is None or len(data) == 0:
        logger.warn('data is None or len(data) == 0')
        abort(400)

    data = BytesIO(data)
    thumb = BytesIO()
    ImageOps.fit(Image.open(data), PHOTO_THUMBNAIL_SIZE, Image.ANTIALIAS).save(thumb, 'JPEG')
    thumb.seek(0)
    thumb_id = fs.put(thumb, 
        filename='thumb_user_edited',
        contentType=content_type,
        is_thumbnail=True
    )
    id = fs.put(data, 
        filename='user_edited', 
        contentType=content_type,
        unit_id=unit_id,
        event_id=event_id,
        import_key=None,
        thumb_id=thumb_id,
        # thumb_id=None,
        code=gen_photo_code(), # TODO: this function takes 2 params
        uuid=uuid.uuid4().hex
    )
    upload_img(id)
    return 'ok\n'


@app.route('/photos/<id>/image', methods=['GET'])
# @benchmark
def get_photo_image(id):
    f = fs.get(ObjectId(id))
    if not f:
        abort(404)
    return Response(f, 
        mimetype=f.content_type,
        headers=[('Content-Length', '{}'.format(f.length))],
        direct_passthrough=True)


@app.route('/photos/<id>/thumbnail', methods=['GET'])
# @benchmark
def get_photo_thumbnail(id):
    f = fs.get(ObjectId(id))
    if not f or not f.thumb_id:
        abort(404)
    thumb = fs.get(f.thumb_id)
    return Response(thumb, 
        mimetype=thumb.content_type,
        headers=[('Content-Length', '{}'.format(thumb.length))],
        direct_passthrough=True)


@app.route('/photos/<id>/print', methods=['POST'])
# @benchmark
def print_photo(id):
    logger.debug('printing photo, id: {}'.format(id))
    print_img(ObjectId(id))
    return 'ok'


@app.route('/photos/<id>/print_ticket', methods=['POST'])
# @benchmark
def print_ticket(id):
    logger.debug('printing ticket, id: {}'.format(id))
    print_tkt(ObjectId(id))
    return 'ok'


def update_unit_last_seen(unit_id, addr):
    db.units.update_one({'_id': unit_id}, 
        {'$set': {
            'lastSeen': arrow.utcnow().datetime,
            'addr': addr
        }}, upsert=True)


@app.route('/photos/last', methods=['GET'])
# @benchmark
def get_last_photo():
    unit_id = request.values['unit_id'].strip()
    update_unit_last_seen(unit_id, request.remote_addr)
    event_id = get_default_event()['_id']
    most_recent_file = _get_last_photo(event_id, unit_id)
    if most_recent_file is not None:
        r = '{}\n'.format(most_recent_file['import_key'])
        logger.debug('get_last_photo: {}'.format(r))
        return r
    else:
        logger.debug('get_last_photo: empty')
        return ' \n'


### Printers

@app.route('/printers', methods=['GET'])
# @benchmark
def list_printers():
    return jsonify({
        'printers': printing.enum_printers(),
        'photo_printer': get_photo_printer(),
        'ticket_printer': get_ticket_printer(),
    })


@app.route('/printers/photo', methods=['POST'])
# @benchmark
def set_photo_printer():
    photo_printer = request.json['photo_printer']
    db.settings.update_one({'_id': 1}, 
        {'$set': {'photo_printer': photo_printer}}, 
        upsert=True)
    return ''


@app.route('/printers/ticket', methods=['POST'])
# @benchmark
def set_ticket_printer():
    ticket_printer = request.json['ticket_printer']
    db.settings.update_one({'_id': 1}, 
        {'$set': {'ticket_printer': ticket_printer}}, 
        upsert=True)
    return ''


### Events

@app.route('/events', methods=['GET'])
# @benchmark
def list_events():
    return jsonify({
        'events': enum_events(),
        'default_event': get_default_event()
    })

@app.route('/events/default', methods=['POST'])
# @benchmark
def set_default_event():
    event = request.json['event']
    db.settings.update_one({'_id': 1}, {'$set': {'default_event': event}}, upsert=True)
    try:
        r = requests.get(app.config['NOWCAM_CENTRAL_URL'] + '/events/' + event['_id'] + '/ticket_body', 
            headers=AUTH_HDRS, verify=VERIFY_SSL)
        r.raise_for_status()
        id = fs.put(BytesIO(r.content))
        logger.debug('updating event ticket')
        db.settings.update_one({'_id': 1}, {'$set': {'default_event.ticket_image_id': id}})
    except Exception as ex:
        logger.warn(ex)
    try:
        r = requests.get(app.config['NOWCAM_CENTRAL_URL'] + '/events/' + event['_id'] + '/overlay', 
            headers=AUTH_HDRS, verify=VERIFY_SSL)
        r.raise_for_status()
        id = fs.put(BytesIO(r.content))
        logger.debug('updating event overlay')
        db.settings.update_one({'_id': 1}, {'$set': {'default_event.overlay': id}})
    except Exception as ex:
        logger.warn(ex)
    return ''


### Camera

@app.route('/camera', methods=['GET'])
# @benchmark
def get_camera():
    ip = request.values['ip']
    state = camera.check_wifisd(ip)
    return jsonify({
        'ip': ip,
        'state': state
    })


@app.route('/camera/launch_broadcast_listener', methods=['GET'])
# @benchmark
def launch_broadcast_listener():
    ip = request.values['ip']
    state = camera.launch_broadcast_listener(ip)
    return ''


@app.route('/units', methods=['GET'])
# @benchmark
def list_units():
    return jsonify({
        'items': [u for u in db.units.find({
            'lastSeen': {'$gt': arrow.utcnow().replace(days=-1).datetime}})],
        'total': db.units.count()
    })


@app.route('/video-gifs-settings', methods=['GET'])
def get_video_gifs_settings():
    return jsonify({
        'video_gifs_settings': db.settings.find_one({'_id': 1}).get('video_gifs_settings', '')
    })


@app.route('/video-gifs-settings', methods=['POST'])
def set_video_gifs_settings():
    video_gifs_settings = request.json['video_gifs_settings']
    db.settings.update_one({'_id': 1}, 
        {'$set': {'video_gifs_settings': video_gifs_settings}}, 
        upsert=True)
    return ''    


## Error handlers

@app.errorhandler(400)
def error_not_found(err):
    return make_response(jsonify({
        'error': 'Bad request',
        'description': err.description
    }), 400)


@app.errorhandler(404)
def error_not_found(err):
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.errorhandler(405)
def error_not_found(err):
    return make_response(jsonify({
        'error': 'Method not allowed',
        'description': err.description
    }), 405)


@app.errorhandler(415)
def error_not_found(err):
    return make_response(jsonify({
        'error': 'Unsupported media type',
        'description': err.description
    }), 415)


@app.errorhandler(Exception)
def error_server_error(err):
    logger.exception('Error')
    return make_response(jsonify({
        'error': 'Server error',
        'description': str(err)
    }), 500)





