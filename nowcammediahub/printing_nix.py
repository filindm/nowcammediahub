from PIL import Image
import subprocess


def print_image(name, file, printer_name):
    # print 'printing ' + name
    stdin = subprocess.Popen(["lpr", "-P", printer_name, "-T", name], 
        stdin=subprocess.PIPE).stdin
    file.seek(0)
    buf = file.read(8192)
    while buf:
        stdin.write(buf)
        buf = file.read(8192)
    stdin.close()


def print_ticket(code, img_file, ticket_img_file, printer_name, text_box):
    pass


def enum_printers():
    return map(lambda x: x.split(' ')[1], filter(None, subprocess.check_output(['lpstat', '-p']).split('\n')))


if __name__ == '__main__':
    print_image('test.jpg', open('test.jpg', 'rb'))

