import time
import os
import signal
import uuid
import requests
import arrow


def process_file(filepath, filename, group=None):
    if filename[-4:].lower() in ['.jpg', '.gif']:
        remove_file = False
        with open(filepath, 'rb') as f:
            print filepath
            try:
                data={'unit_id': '001', 'key': str(arrow.now())}
                if group:
                    data['group'] = group
                r = requests.post('http://localhost:5010/photos', files={'file': f}, data=data)
                r.raise_for_status()
                # print 'status code:', r.status_code
                if r.status_code == 200:
                    remove_file = True
            except Exception as ex:
                print ex
        if remove_file:
            os.unlink(filepath)


def process_dir(dirpath, dirname):
    if not dirname.startswith('.'):
        for filename in os.listdir(dirpath):
            filepath = os.path.join(dirpath, filename)
            if os.path.isfile(filepath):
                process_file(filepath, filename, group=dirname)


def main():
    if os.name == 'posix':
        dirname = os.path.expanduser('~/NowcamTetheredCamera/img_processing')
    else:
        dirname = 'C:\\NowcamTetheredCamera\\img_processing'
    try:
        while True:
            for filename in os.listdir(dirname):
                filepath = os.path.join(dirname, filename)
                if os.path.isfile(filepath):
                    process_file(filepath, filename)
                elif os.path.isdir(filepath):
                    process_dir(filepath, filename)
            time.sleep(1.0)
    except KeyboardInterrupt:
        pass
        
        
if __name__ == '__main__':
    main()

