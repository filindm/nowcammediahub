/*
*   Application
*/
angular.module('app', [
    'ngRoute',
]);

/*
*   MainCtrl
*/
angular.module('app').controller('MainCtrl', 
    ['$scope', '$http', function($scope, $http){

    $scope.tabs = [{
        'title': 'General Settings',
        'url': '/static/partials/tabs.general.html'
    },{
        'title': 'Photos',
        'url': '/static/partials/tabs.photos.html'
    }];

    $scope.activeTab = $scope.tabs[0];

    $scope.setActiveTab = function(tab){
        $scope.activeTab = tab;
    }

    $scope.isActiveTab = function(tab){
        return $scope.activeTab === tab;
    }

    $scope.refresh = function(){
        $scope.$broadcast('refresh');
    }

}]);


/*
*  Router
*/
angular.module('app').config(['$routeProvider', function($routeProvider){

    $routeProvider

    .when('/main', {
        templateUrl: 'static/partials/main.html',
        controller: 'MainCtrl'
    })
    .otherwise({
        redirectTo: '/main'
    })

}]);

/*
*   TabsGeneralCtrl
*/
angular.module('app').controller('TabsGeneralCtrl', 
    ['$scope', '$http', '$interval', function($scope, $http, $interval){

    refresh();

    var refreshTimer = $interval(function(){
        if($scope.autoRefresh){
            refresh();
        }
    }, 3000);

    $scope.$on('$destroy', function(){
        $interval.cancel(refreshTimer);
    })

    $scope.$on('refresh', function(){
        refresh();
    })

    function refresh(){
        load_printers();
        load_events();
        load_units();
        load_video_gifs_settings();
    }

    function load_printers(){
        $http.get('/printers').then(function(res){
            $scope.printers = res.data.printers;
            $scope.photo_printer = res.data.photo_printer;
            $scope.ticket_printer = res.data.ticket_printer;
        }, function(err){
            $scope.error = err;
        })
    }

    function load_events(){
        $http.get('/events').then(function(res){
            $scope.events = res.data.events;
            $scope.default_event = res.data.default_event;
        }, function(err){
            $scope.error = err;
        })
    }

    function load_units(){
        $http.get('/units').then(function(res){
            $scope.units = res.data.items;
        }, function(err){
            $scope.error = err;
        })
    }

    function load_video_gifs_settings(){
        $http.get('/video-gifs-settings').then(function(res){
            console.log(res.data)
            $scope.video_gifs_settings = res.data.video_gifs_settings;
        }, function(err){
            $scope.error = err;
        })
    }

    $scope.submit_photo_printer = function(){
        $http.post('/printers/photo', {
            photo_printer: $scope.photo_printer
        }).then(function(){
            $scope.theFormPhotoPrinter.$setPristine();
            load_printers();
        }, function(err){
            $scope.error = err;
        })
    }

    $scope.submit_ticket_printer = function(){
        $http.post('/printers/ticket', {
            ticket_printer: $scope.ticket_printer
        }).then(function(){
            $scope.theFormTicketPrinter.$setPristine();
            load_printers();
        }, function(err){
            $scope.error = err;
        })
    }

    $scope.submit_event = function(){
        $http.post('/events/default', {
            event: $scope.default_event
        }).then(function(){
            $scope.theFormEvents.$setPristine();
            load_events();
        }, function(err){
            $scope.error = err;
        })
    }

    $scope.submit_video_gifs_settings = function(){
        $http.post('/video-gifs-settings', {
            video_gifs_settings: $scope.video_gifs_settings
        }).then(function(){
            $scope.theFormVideoGIF.$setPristine();
            load_video_gifs_settings();
        }, function(err){
            $scope.error = err;
        })
    }

    $scope.cancel_photo_printer = function(){
        $scope.theFormPhotoPrinter.$setPristine();
        load_printers();
    }

    $scope.cancel_ticket_printer = function(){
        $scope.theFormTicketPrinter.$setPristine();
        load_printers();
    }

    $scope.cancel_event = function(){
        $scope.theFormEvents.$setPristine();
        load_events();
    }

    $scope.cancel_video_gifs_settings = function(){
        $scope.theFormVideoGIF.$setPristine();
        load_video_gifs_settings();
    }

    $scope.connect_to_camera = function(){
        $http.get('/camera', {params: {ip: $scope.camera.ip}}).then(function(res){
            $scope.camera.state = res.data.state;
        }, function(err){
            $scope.error = err;
        })
    }

    $scope.launch_broadcast_listener = function(){
        $http.get('/camera/launch_broadcast_listener', 
            {params: {ip: $scope.camera.ip}}).then(function(res){
            $scope.connect_to_camera();
        }, function(err){
            $scope.error = err;
        })
    }

    $scope.isUnitOnline = function(lastSeen){
        // return (new Date() - new Date(lastSeen)) < 5000;
        var d1 = new Date(lastSeen);
        var d2 = new Date();
        // console.log(d1, ' :: ', d2, ' :: ', (d2 - d1)/1000);
        // console.log(d2 - d1);
        return (d2 - d1) < 5000;
    }

    $scope.toDate = function(s){
        // console.log(s);
        return new Date(s);
    }

    $scope.humanizeDate = function(s){
        return moment(s).fromNow();
    }

}]);


/*
*   TabsPhotosCtrl
*/
angular.module('app').controller('TabsPhotosCtrl', 
    ['$scope', '$http', '$interval', function($scope, $http, $interval){

    var photos_per_page = 15;
    var max_pages_count = 15;
    $scope.current_page = 0;
    $scope.start_page = 0;

    $scope.toDate = function(s){
        return new Date(s);
    }

    $scope.print_photo = function(photo){
        $http.post('/photos/' + photo._id + '/print');
    }

    $scope.print_ticket = function(photo){
        $http.post('/photos/' + photo._id + '/print_ticket');
    }

    $scope.total_pages = function(){
        return Math.ceil($scope.photos.total / photos_per_page);
    }

    $scope.has_prev = function(){
        return $scope.start_page > 0;
    }

    $scope.has_next = function(){
        return $scope.start_page + max_pages_count < $scope.total_pages();
    }

    $scope.enum_pages = function(){
        var res = [];
        for(var i = $scope.start_page; 
            i < $scope.start_page + max_pages_count && i < $scope.total_pages(); 
            i++){
            res.push(i);
        }
        return res;
    }

    $scope.prev = function(){
        if($scope.has_prev()){
            $scope.start_page -= max_pages_count;
            $scope.current_page = $scope.start_page + max_pages_count - 1;
            load_photos();
        }
    }

    $scope.next = function(){
        if($scope.has_next()){
            $scope.start_page += max_pages_count;
            $scope.current_page = $scope.start_page;
            load_photos();
        }
    }

    $scope.set_current_page = function(n){
        $scope.current_page = n;
        load_photos();
    }


    load_photos();

    var refreshTimer = $interval(function(){
        if($scope.autoRefresh){
            load_photos();
        }
    }, 3000);

    $scope.$on('$destroy', function(){
        $interval.cancel(refresh);
    })

    $scope.$on('refresh', function(){
        load_photos();
    })

    $scope.$watch('code', function(){
        load_photos();
    })

    function load_photos(){
        var params = {};
        if($scope.code){
            params.code = $scope.code.toUpperCase();
        } else {
            params.skip = $scope.current_page * photos_per_page;
            params.limit = photos_per_page;
        }
        $http.get('/photos', {params: params}).then(function(resp){
            $scope.photos = resp.data;
        }, function(err){
            $scope.error = err;
        })
    }

}]);


