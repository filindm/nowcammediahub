/*
*   Application
*/
angular.module('app', [
    'ngRoute',
]);


/*
*  Router
*/
angular.module('app').config(['$routeProvider', function($routeProvider){

    $routeProvider
    .when('/', {
        templateUrl: 'static/partials/rfid.main.html',
        controller: 'MainCtrl'
    })
    .when('/form', {
        templateUrl: 'static/partials/rfid.form.html',
        controller: 'FormCtrl'
    })
    .otherwise({
        redirectTo: '/'
    })
}]);


/*
*   MainCtrl
*/
angular.module('app').controller('MainCtrl', 
    ['$scope', '$http', '$interval', '$location', function($scope, $http, $interval, $location){

    var running = false;
    var timer = $interval(function(){
        if(!running){
            $http.get('/api/rfid').then(function(res){
                if(res.data && res.data.rfid){
                    console.log('rfid: ' + JSON.stringify(res.data.rfid));
                    var rfid = res.data.rfid.trim();
                    if(rfid){
                        //console.log('rfid: ' + rfid);
                        $location.path('/form');
                    }
                }
            }, function(err){
                console.error(err);
            }).finally(function(){
                running = false;
            })
            running = true;
        }
    }, 1000);

    $scope.$on('$destroy', function(){
        $interval.cancel(timer);
    })

}]);


/*
*   FormCtrl
*/
angular.module('app').controller('FormCtrl', 
    ['$scope', '$http', '$location', function($scope, $http, $location){

    $scope.data = {
        firstName: '',
        lastName: '',
        email: ''
    };

    $scope.onSubmit = function(){
        // console.log(JSON.stringify($scope.data));
        $http.post('/api/user_data', $scope.data).then(function(res){
            $location.path('/');
        }, function(err){
            console.error(err);
        })
    }

    function loadFbSdk() {
        window.fbAsyncInit = function() {
            FB.init({
                appId  : '1407263872877227',
                version: 'v2.5',
                status: true,
            });
            console.log('init ok');
            FB.getLoginStatus(function(resp) {
                if(resp.status === 'connected'){
                    // console.log('access token: ' + resp.authResponse.accessToken);
                    $scope.data.accessToken = resp.authResponse.accessToken;
                } else {
                    FB.login(function(resp){
                        if(resp.status === 'connected'){
                            // console.log('access token: ' + resp.authResponse.accessToken);    
                            $scope.data.accessToken = resp.authResponse.accessToken;
                        }
                    })
                }
            });
        };
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#version=v2.5";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }

    loadFbSdk();

}]);


