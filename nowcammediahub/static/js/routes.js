/*
*  Router
*/
angular.module('app').config(['$routeProvider', function($routeProvider){

    $routeProvider

    .when('/main', {
        templateUrl: 'static/partials/main.html',
        controller: 'MainCtrl'
    })
    .otherwise({
        redirectTo: '/main'
    })

}]);

