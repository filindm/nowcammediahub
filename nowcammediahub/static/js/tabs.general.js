/*
*   TabsGeneralCtrl
*/
angular.module('app').controller('TabsGeneralCtrl', 
    ['$scope', '$http', '$interval', function($scope, $http, $interval){

    refresh();

    var refreshTimer = $interval(function(){
        if($scope.autoRefresh){
            refresh();
        }
    }, 3000);

    $scope.$on('$destroy', function(){
        $interval.cancel(refreshTimer);
    })

    $scope.$on('refresh', function(){
        refresh();
    })

    function refresh(){
        load_printers();
        load_events();
        load_units();
    }

    function load_printers(){
        $http.get('/printers').then(function(res){
            $scope.printers = res.data.printers;
            $scope.photo_printer = res.data.photo_printer;
            $scope.ticket_printer = res.data.ticket_printer;
        }, function(err){
            $scope.error = err;
        })
    }

    function load_events(){
        $http.get('/events').then(function(res){
            $scope.events = res.data.events;
            $scope.default_event = res.data.default_event;
        }, function(err){
            $scope.error = err;
        })
    }

    function load_units(){
        $http.get('/units').then(function(res){
            $scope.units = res.data.items;
        }, function(err){
            $scope.error = err;
        })
    }

    $scope.submit_photo_printer = function(){
        $http.post('/printers/photo', {
            photo_printer: $scope.photo_printer
        }).then(function(){
            $scope.theFormPhotoPrinter.$setPristine();
            load_printers();
        }, function(err){
            $scope.error = err;
        })
    }

    $scope.submit_ticket_printer = function(){
        $http.post('/printers/ticket', {
            ticket_printer: $scope.ticket_printer
        }).then(function(){
            $scope.theFormTicketPrinter.$setPristine();
            load_printers();
        }, function(err){
            $scope.error = err;
        })
    }

    $scope.submit_event = function(){
        $http.post('/events/default', {
            event: $scope.default_event
        }).then(function(){
            $scope.theFormEvents.$setPristine();
            load_events();
        }, function(err){
            $scope.error = err;
        })
    }

    $scope.cancel_photo_printer = function(){
        $scope.theFormPhotoPrinter.$setPristine();
        load_printers();
    }

    $scope.cancel_ticket_printer = function(){
        $scope.theFormTicketPrinter.$setPristine();
        load_printers();
    }

    $scope.cancel_event = function(){
        $scope.theFormEvents.$setPristine();
        load_events();
    }

    $scope.connect_to_camera = function(){
        $http.get('/camera', {params: {ip: $scope.camera.ip}}).then(function(res){
            $scope.camera.state = res.data.state;
        }, function(err){
            $scope.error = err;
        })
    }

    $scope.launch_broadcast_listener = function(){
        $http.get('/camera/launch_broadcast_listener', 
            {params: {ip: $scope.camera.ip}}).then(function(res){
            $scope.connect_to_camera();
        }, function(err){
            $scope.error = err;
        })
    }

    $scope.isUnitOnline = function(lastSeen){
        // return (new Date() - new Date(lastSeen)) < 5000;
        var d1 = new Date(lastSeen);
        var d2 = new Date();
        // console.log(d1, ' :: ', d2, ' :: ', (d2 - d1)/1000);
        // console.log(d2 - d1);
        return (d2 - d1) < 5000;
    }

    $scope.toDate = function(s){
        // console.log(s);
        return new Date(s);
    }

    $scope.humanizeDate = function(s){
        return moment(s).fromNow();
    }

}]);


