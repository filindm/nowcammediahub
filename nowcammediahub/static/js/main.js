/*
*   MainCtrl
*/
angular.module('app').controller('MainCtrl', 
    ['$scope', '$http', function($scope, $http){

    $scope.tabs = [{
        'title': 'General Settings',
        'url': '/static/partials/tabs.general.html'
    },{
        'title': 'Photos',
        'url': '/static/partials/tabs.photos.html'
    }];

    $scope.activeTab = $scope.tabs[0];

    $scope.setActiveTab = function(tab){
        $scope.activeTab = tab;
    }

    $scope.isActiveTab = function(tab){
        return $scope.activeTab === tab;
    }

    $scope.refresh = function(){
        $scope.$broadcast('refresh');
    }

}]);


