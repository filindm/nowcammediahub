/*
*   TabsPhotosCtrl
*/
angular.module('app').controller('TabsPhotosCtrl', 
    ['$scope', '$http', '$interval', function($scope, $http, $interval){

    var photos_per_page = 15;
    var max_pages_count = 15;
    $scope.current_page = 0;
    $scope.start_page = 0;

    $scope.toDate = function(s){
        return new Date(s);
    }

    $scope.print_photo = function(photo){
        $http.post('/photos/' + photo._id + '/print');
    }

    $scope.print_ticket = function(photo){
        $http.post('/photos/' + photo._id + '/print_ticket');
    }

    $scope.total_pages = function(){
        return Math.ceil($scope.photos.total / photos_per_page);
    }

    $scope.has_prev = function(){
        return $scope.start_page > 0;
    }

    $scope.has_next = function(){
        return $scope.start_page + max_pages_count < $scope.total_pages();
    }

    $scope.enum_pages = function(){
        var res = [];
        for(var i = $scope.start_page; 
            i < $scope.start_page + max_pages_count && i < $scope.total_pages(); 
            i++){
            res.push(i);
        }
        return res;
    }

    $scope.prev = function(){
        if($scope.has_prev()){
            $scope.start_page -= max_pages_count;
            $scope.current_page = $scope.start_page + max_pages_count - 1;
            load_photos();
        }
    }

    $scope.next = function(){
        if($scope.has_next()){
            $scope.start_page += max_pages_count;
            $scope.current_page = $scope.start_page;
            load_photos();
        }
    }

    $scope.set_current_page = function(n){
        $scope.current_page = n;
        load_photos();
    }


    load_photos();

    var refreshTimer = $interval(function(){
        if($scope.autoRefresh){
            load_photos();
        }
    }, 3000);

    $scope.$on('$destroy', function(){
        $interval.cancel(refreshTimer);
    })

    $scope.$on('refresh', function(){
        load_photos();
    })

    $scope.$watch('code', function(){
        load_photos();
    })

    function load_photos(){
        var params = {};
        if($scope.code){
            params.code = $scope.code.toUpperCase();
        } else {
            params.skip = $scope.current_page * photos_per_page;
            params.limit = photos_per_page;
        }
        $http.get('/photos', {params: params}).then(function(resp){
            $scope.photos = resp.data;
        }, function(err){
            $scope.error = err;
        })
    }

}]);


