$(function(){
    console.log('container: w: ' + $('#photo-canvas-container').width() + ', h: ' + $('#photo-canvas-container').height());
    console.log('window: w: ' + $(window).width() + ', h: ' + $(window).height());
})

/*
*   Application
*/
angular.module('app', [
    'ngRoute',
]);


/*
*  Router
*/
angular.module('app').config(['$routeProvider', function($routeProvider){

    $routeProvider
    .when('/', {
        templateUrl: 'static/partials/edit.gallery.html',
        controller: 'GalleryCtrl'
    })
    .when('/photo/:id', {
        templateUrl: 'static/partials/edit.photo.html',
        controller: 'PhotoCtrl'
    })
    .otherwise({
        redirectTo: '/'
    })
}]);


/*
*   GalleryCtrl
*/
angular.module('app').controller('GalleryCtrl', 
    ['$scope', '$http', '$interval', function($scope, $http, $interval){

    var photos_per_page = 15;
    var max_pages_count = 15;
    $scope.current_page = 0;
    $scope.start_page = 0;
    $scope.code = '';

    // $scope.toDate = function(s){
    //     return new Date(s);
    // }

    // $scope.print_photo = function(photo){
    //     $http.post('/photos/' + photo._id + '/print');
    // }

    $scope.total_pages = function(){
        return Math.ceil($scope.photos.total / photos_per_page);
    }

    $scope.has_prev = function(){
        return $scope.start_page > 0;
    }

    $scope.has_next = function(){
        return $scope.start_page + max_pages_count < $scope.total_pages();
    }

    $scope.enum_pages = function(){
        var res = [];
        for(var i = $scope.start_page; 
            i < $scope.start_page + max_pages_count && i < $scope.total_pages(); 
            i++){
            res.push(i);
        }
        return res;
    }

    $scope.prev = function(){
        if($scope.has_prev()){
            $scope.start_page -= max_pages_count;
            $scope.current_page = $scope.start_page + max_pages_count - 1;
            load_photos();
        }
    }

    $scope.next = function(){
        if($scope.has_next()){
            $scope.start_page += max_pages_count;
            $scope.current_page = $scope.start_page;
            load_photos();
        }
    }

    $scope.set_current_page = function(n){
        $scope.current_page = n;
        load_photos();
    }

    load_photos();

    var refresh = $interval(function(){
        if($scope.autoRefresh){
            load_photos();
        }
    }, 3000);

    $scope.$on('$destroy', function(){
        $interval.cancel(refresh);
    })

    $scope.$on('refresh', function(){
        load_photos();
    })

    $scope.$watch('code', function(){
        load_photos();
    })

    function load_photos(){
        var params = {};
        if($scope.code){
            params.code = $scope.code.toUpperCase();
        } else {
            params.skip = $scope.current_page * photos_per_page;
            params.limit = photos_per_page;
        }
        $http.get('/photos', {params: params}).then(function(resp){
            $scope.photos = resp.data;
        }, function(err){
            $scope.error = err;
        })
    }

}]);


/*
*   PhotoCtrl
*/
angular.module('app').controller('PhotoCtrl', 
    ['$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location){

    var photo_id = $routeParams.id;
    var canvas = document.getElementById('photo-canvas');
    var ctx = canvas.getContext("2d");
    var SPEED = 1;

    $scope.text1 = {
        contents: 'Hello, World!',
        top: 100,
        left: 200,
        fontSize: 96,
        color: 'white',
        moveLeft: moveLeft(),
        moveRight: moveRight(),
        moveUp: moveUp(),
        moveDown: moveDown()
    }
    $scope.$watch('text1.contents', function(){
        drawImage();
    });
    $scope.$watch('text1.left', function(){
        drawImage();
    });
    $scope.$watch('text1.top', function(){
        drawImage();
    });

    $scope.text2 = {
        contents: 'Merry Xmas!',
        top: 300,
        left: 200,
        fontSize: 96,
        color: 'red',
        moveLeft: moveLeft(),
        moveRight: moveRight(),
        moveUp: moveUp(),
        moveDown: moveDown()
    }
    $scope.$watch('text2.contents', function(){
        drawImage();
    });
    $scope.$watch('text2.left', function(){
        drawImage();
    });
    $scope.$watch('text2.top', function(){
        drawImage();
    });

    $scope.overlay = {
        imgs: [
            createImage('http://akvis.com/img/examples/smartmask/wildlife-photo/deer-transparent-background.jpg'),
            createImage('http://cdn2.arkive.org/media/EB/EB5BC604-FBB2-49E5-AF95-3D1CE7ACE28E/Presentation.Large/Red-kangaroo-hopping.jpg'),
            createImage('https://lh6.ggpht.com/GroSPPPMmqrydWFVKG_tOvgAlh3VB6qW6oqOm9cbkA5lWFukjtoimjJJnhKKc1xcxA=w300-rw'),
            createImage('http://www.thetimes.co.uk/tto/multimedia/archive/00309/108787995_309592c.jpg'),
            createImage('http://data.whicdn.com/images/14922648/large.jpg'),
            createImage('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTExMVFRUXFxUXFxcYGBcXFxcXFxcXFxcYFxUYHSggGBolHRUXITEiJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGi0lHx0tLS0tLS8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAKgBLAMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAEAAECAwUGB//EADoQAAEDAgQDBgUDAgYDAQAAAAEAAhEDIQQSMUEFUWETInGBkaEGMsHR8BRCsVLhBxUWI2LxU3KCM//EABoBAAIDAQEAAAAAAAAAAAAAAAECAAMEBgX/xAAtEQACAgEEAQMCBQUBAAAAAAAAAQIRAwQSITEFE0FRcYEUI1Jh4SIyQqHxFf/aAAwDAQACEQMRAD8A5FxThRKcLtUcqOphRDk7XopIV2SThqYuU2OndMkhXYnwEi0gJFgSY6D0RoFllIyP73UuxMAiEmhjuienRJPdmfZSTjFWwRUpOo9lYIm4jwUssmM0+61KfDCbujwkD6oyngiB3WW6R9Fgy+QxR65N+Lx2WXMuDIp4LnA8bfwrv8sb/WPQrS/T87KLsN+XWGfkpvpG2Hjca7bZn/5U3Z6ieEu2fP50KKqUCFDKfJVryWRdlj8diAanDqg2nwt/KFewjUEeK2BWeNCfA3VjcVNnNB8Fox+Uj/kjPPxn6WYjHBXCmFo1OH033acp/NkK7AvZ1HS/svSxanFk6Z5ubS5cfNFGRIiNFYFFwWrajJbIkp00KWXdSiWMWwo5SpypI7US2QgqLiVdChkQ2gUimVNoKRCUwhQ1jZSplyiaiRKNEGLZ0SDeqQSKFBscBOSOqimUALtCmaUinYg0OgEplIpQqkXjJwknTAJwk2yi1TDZMBFtJWwU26QlNtMlEYagNSjKbZOi83N5GMXUOT0MPj5S5m6BsNhJ1ut7BYSBZU4SgJkrREBeVl1M8r5Z6eLTwxL+lDuo+CgWkaH0TuqqsvndUui5F1PEHe46q59IG8LNcb6onC4o6FCwiqYdUPwfRHvbPK6iWnxHv6qUQz/0aY4Balv+9fVTawI7USzFOFhWU5FtQtoYWUJisCW3hNVcoW7AqmBa/Sx9/wC6ycVg3M1uPf0Wy0kIgVA8Qb9fzVbdPrsmPh8oxajQ48vKVM5JSLfyVpcS4dBzN8/us4tC97DmjljuieBnwywy2yIx4JEG30ScIS8FcVDGoVNr1CUzWqEaRY8hNkndItSaSLog+hUQnancU7QhQwspUZVrinzSpQLKJTqfZpjSKgbREhM0J4KdpQaCjOBUpUHBIFZkamiwJAppRGEwpeeg1P0CkpqKtkhBydIbD0C7oOf2R9OjAMbAnzhEtogC1gFZTbuvB1WslkdLo9vTaSONW+wOkyALbWvPoi6Aj7Jsg5AeAhTavPuzakGUVfBIVeFR1MKxIAG5pUPJagYndSCO0FmTnHJRhbJwrDqAoDhzNiUNrDZn08QQiGVpV54UD+6FW7hLho4H2Upolom2ormFBnBVR+2fC6lTJHzA+kfyimQ3MK2RKLNKQsjDYxH0uIt6K1MRoExvCgZhZNfBuYV2eFxlBxAeI6gn3UDhqVRzgKgDbwXW90jaCrOKbVBs5Y3FcDlOYab/AHXY43hbXTlInoQfcLFrYV7e64GPortPqJYJ37FGo08c0KfZzBKdrr9Fo1uHibGPJQp8OH9R9F7q8hgq9x4b0Ge62gWVJtlqN4YObvZWf5UP+Xso/Jaf9Qf/ADdQ/b/ZkG15lOtYcMbyd4yo1OFH9pnxQj5PTSdbgT8bqIq6MnyUiLKx9MiQ4QeShELeqfKMD4dMZrZ2TRCsaFXN0wETUSp5Qq3aoEQr7Kym2ygVNroQaCBupNEieaEDCTa6OdQzEhu0orDYUCBEuJuelrRtosGozwxql2ejpsEsnL6Bm8OsL+35C1KVMNaALAKbxcBM8bLxc+olNUz2sOnhB2hAppUssWTliwXZroqlSBScxJo6KUQNwruS0IA0Ow1teBPus/DgIprQrExQxhVzChGkJjiOSe0AMdCZtVBdsYuph8oWQMFVWBxQtIFEhqZAZPtOqbtimyKutUa3U/dQBcGsOrRPMWPsgcVUYPlM+49UPWxJdYach9VBtAnUwlb+Ai7a/dlX9uWiST4KipVa3RDF5N0LoIdSxB1mFdi8eakFxJgBo/8AUaLPaLSlUZyKFhI1Kg0CTXBQYRMQrDAShLWOHNE07oCm6T0RzKgSsJo4HBB4dmcGw0kT+7oOqDxJYxpmBG87IbFcUbTYS50Ac1w3FeLvxT+zYSGfuO5HXl4I48Tyy2xFyZVjjukaWI4iK1Q5Plbvz8EmqjDUgxoa0WCvgkTC7LTYXixRg30chqcvq5HPqxOMKsG6lCcjTT18vorylCzKIG6aegTFxUDRMKTXBUVJsrGjogSjbxeE7NrZAGYggC4gXku3NvJUMsJOt/fVTpuNRwlsDvbkkACQLkAC2ysZhpB9Fy03Z1kUD0GZrqxrIK0MNgQNFI4a8QseRNmiJS3DggcyfZP+kKMo0gpVacIqPBGwA4fmnZhxsinGQqmoNEGa2EnFM83VbnKMgjUTApNuiKdFRIhCm0lF0qaVNgCtJTJAZaxoSq1g25KAxPEA2wuUIJd3nFFy+AUGvxzjZv557Kp1OfmPl/dRNQAckHicbGiD/cIaagbyQWIxuyAqYgn6KeHw5Nyg2Ci1hJN0U0FM1obruovrHb+6QYtqPtCFq14sFS+rCodVQbCEOq+qY1uqE7RVVKoGpQIaDMShsZxkMEk/dY+I4lPdYMx6J8Jw4znqXdsNh/da9Nop53S6+TJqdZDCue/gQa+uc9Wcv7W/UoynQa0d0AeCuIScYC6jT6XHgjUV9zm8+pnmlcmVuCsonY+iiLhO0bLQUPoeubqkqyqLqEoBj0OCEimKZQI+ZSaeQUC5SBUCkdtw/g1R1Muphvdkhx3LQJBmwEx5NKFZTDTlAtJjnlnun0j1RlLiDabZdJFOoxoaSR3HOAc8OBsQCBcxPNE8bw7GljqbgWukBoIdly2AMG9oN7rkrp0zrQemy0qxsGYVNJ9vzRPTO6RrkZMQsVRiaiVR90LXqSkbGEaijmVWZSa6VXYxJzkqbZsmaLq6m2FAE6NNFMaq6QV+yeICtxhZuOxv7R5qziGJiwN1m0RuUHIJdRZudVKpXhUvq/2QOIrQlAXYjFIQOLj0QznEmEdh6egUAE4an0Rxa6LWTUGQrC9QYpeEPVeToiKr0BiMUAgyEXt5oDEYkNN1n8U42GzcSubxPEXOnfe/2Ur5BfwdJiOK7Nlx6JUMI9xmoYGw1XJDFPBBkjcbeELo+E/EIMNqHzW/RLTyl+YYda9Qo3j/AJN7DYRrB3QPHc+asc0qsVmm7TIUwungopVHo5mTk3cuxOKiW2UjyT5U4Log2yXaKuoUqXJCxtvuO50mU0KWRRKIUPKdsKITqELAFIAKuVIE7IMCR1bnxlcJaQba5XlkEgNbGhgR4EybKeDxbKnaUaTXlmYVKDXFrDlNqhe/cC0TeN7IWq1oaRdz7ddHftHKHXjaSqeJcVBqOr0gGvykVGiwcHDK4RvZ+o3v1XJNHXIObVYCWh7XEalpLmnoHHWNE1SpCi3A0ywGk4izrPILnGA4ODoAIIOmoiEOCY/NuaSXVjok6qhnPSLlU56obHQ8q1iHaUVSCUJawoprE1GmiGkDVOkAr0Q2LxoaOuyjxDGBv0WDUrFxkotgCXPzGT/2o1MRH2QdbGBoQja89467dEEgNhtSuQJOp9kEXkqutWSomT0RoW7C8JT3WthacC6ApVGt1UMTxqmyA5wbOhJgePglGRsPrQgsXxFrBLnADquP4h8Vk/8A5g+LvaAufxOJfUMvcT/HolbGOq4j8VtuKYLuugXPYri9Wpq6ByFvdAgKQamimQUSpGgdrp20iUbQpOAiSncYvsgA2qWm/Itg8jsraIYcodbuuDiLXvlK2KWGBEFoI3P5oqcXwUG9I+INx5HVJta6IVYDtG5Ozf8AMwug2Hd281tYPjhhvaMIzjMC28gakgaRC5eX0zNx+XCKwnFMpbb5QWjkAdVqwazLi/tf2MufSYsv9y+52mHxDHiWkO8FcSuVweOp93/hSNMcybQbaRePFa1HFgj5pimLzrUGsTrK9XF5eD4yRo8nL4qadwdhzkmlQjWDo1ro6nULRwvDqry4MZnyhx7t7NAJMeei2w12CXU0ZZ6PNH/FgTnKDFoP4RiS0VOxeWuaXAgCMoOUnW1+alheCVnG4yjr9grHqcSVuSr6iR02V8KLM8iEwRmLwppmHQeRGh+yG6K2E4zjui7TKpxlB7ZKmhiFZTFklEJmKmdTxRjD8pOYuDRIaCWmJhwMteCIBEtPO6yMFTY6sJeInvHlAMibAEkAAzAJE2RjsY0uz9yRmyiDAcTIEQYkmNAFmUGgHNUcGxIabjM/9oeLd0HcdPA8dbOwaRt1AabgWuzNuJc0snn3XTYHdU12nMXAG4kxBDdACSNJJjbUIH9G57g0GDeCTBYS6wDoEeIGgKqxGKbRa4Fxe7MdCTlgZcsuHO87iLI9onQViGlri1wLXCxBsQdwQqXPVVLGmuC90Nc1rcxm7/2tcGm5PygxOs81XmuqOV2WXYdQ5o/BMzGVnUBm0T47iWSWtMBtidJO90Ogm/VeANIWPjuJhthcrFfxaRq6Od499fJZ1TGt5ym3WL0HPxgcSSTPVB4nGgDVZ1fHjZZtWqXGPVPGBXKQa7GFxnbZXU8RaShaVObJcSGQQdeW8p2khOWNVxcnWyieNtaO6C4+gQ9HguJrXFJxG1re60KfwTiiJLAPFzPuqpTvotjD5MnE8ZqvBEgA8tfCUA9xJkkk8yST6ldXT+BsRvkHi8fSUVR+BXfuqNHgHO97Kuk+2OcRCmwjf2Xe0/gmkD3qjj4AD+ZRdH4Pww2eT/7R/ARpLoJwlCg13ym/Lf0RjMFGy7V3whhYAyuF5s4zOmvlop/6SZ3i2s8SO6DGVhnUyCSOk7otsNnIU8EeSJZgjyXXf6cbmYO2cG9n/uHK1x7YA3YP/GTFtRfVSp8CdkYe0Z2hc4PbDwGttlcDlvvOmgSbmHg5ZuFI2VzG9F1P+n3EVP8Acoksy5e9HaAmCW5oiNYKtZ8IVC4APpkGl2jXZhBdlnszezpkToippAqzguKcOa++hXKubFjqLL2U/A9d4pHMxoeSHGQezh0DMAb2vZZz/wDDKq5rnP7IOD4gOAJF4cCDBFo5oSnEiieV5VdSqOGhK9Nd/hOBUc04psZS5joBDjYwf6bE8/lR2D/w7wdPsnPqvqDMRVZoCA79paJAIU9VexNhwPAqD6hkuLWTrufAfVeh8Iz0w51APGVsvcLkNcRck7GB6LWwHwvhKTarG03OBy9m9xgtgkmD1m608TxBgtInI2n3bAsbo0ndJJuQyVGfw+viakMzhrIyyQAA3Ut5AHeUdVxAGYF0uOrtZ81nYniQ2gWQT8c2DJiyrbY/AL8REQT5hYRVXHOLB7gxtzaSiqdOAun8I5ek0+rOZ8y4+omu6K4TsKdwVRK9pnkR5C6omQe6YBtE+Mc1Z2bQLOa5pkjujNOnebtqJEkHqia7qcNDSXGeUFwIBve0XAA1iVNtJuUgi5yGc0ZRIgloBzE6EbCDzC46/c7GgMYns8pABgyLAiRBEAiDEIDHk1HBzobmIaSIaAC43dAgm4vyG8LRxFJokhxsf6QbkgXJNhAN9do3VTazaZFQ94AFhYBqHNcD3rtDoLhJ8bwp9AlOLptoNa+xIYQ5j2wcxlpyHNDokEGTY+SroPzgwIIAJBIkyQ2wm9zttfRavE8ZTfhadRtITDmFheXZiYa24dIyif6dokSBzuCqu7zXBuYNAaZiC27bMMSbgkpatchuma+CxOU3XP43iGVwJguJMZvlaBq4ga/fkj65ZkEVIqgkOYQYG4LX+ndItzK5bjOCfnzEG/mOt9kqhfYdw3EOMZpDXPcf6pyjyaL+pQVLFjK7MHOdBhwdAE5YLhBkCHaROe5sJq/T3iRPK/8AICPwmHbR79Vw6MGrjrB5D+VOUw8E6VB4bcEWmDrfor8JgnnaSVChiXPcXu1cYaPE6rr+GYQwCQrHJoq2pss+HOBAGX3P8LpMD8NU2vNU0w950LrhvgDaeqnwmhGy6LDAReyFWh1wZ/8Al866/myf9Pl2stKrTAi8oWuQdEuxD2ZtZgQFVvJaFU2sgKzgCATcpHElgJqQdFNtdqnWphAVqSG0lmgyuDupgt5rE7QtT/roU2sG43O1HMKbazRuFzx4gFW7HSjsZN6OlFdu5CsbiKe5kLkKmMjQoc496npsm9HcirS5pziqQ3J+3quFPFHhO3iZcCeSnpMnqI7V/FaUyAfNDVviIDQALi6nFZMQq8XinNFwRcgWNyLOjwRWFivKjqK/xC529uSEqcW6rkamNedCme+T+5w9J8hpuro6dsqlqEjdxPHANLrIxXGXvkBV0sG55s2B528ytXBcMa2CRJ9gtmDxzmzHm8gooH4TgyDnfrstgOuok9FZShdBhwxxR2xPCz5XklukIgyqiiqpCEVhXjdmviKrNyMokNuS202Di0GN/Eqp1UhpMnKTJi0ltxpaLx0lUYfK+i0yG1GuJyuDnCo2PmEfLB2i+qEoYpwzN/aQJbcEyQY8iFyFHX2FVKjiNxJ99+s+KmCHghzc2mg3GsjTkdEM3M0i3NoBsdyASecEBJuNDXQ0WJ08zBBG8GEtBTMvG1yXQNGm2wMWm2tgEVintdNUAgkNzCLaASPYqeOwdMkFsydbWI6DYjfxRVGi3si0yMtz/wArKOqAk7OZxZPaOPONOiJoVXxE25G49DKi2lLoMwCtI4QZZ0MAx7fRWWqEp2DYejTLv9yn3SCCWHK4EiA7rBvFpiLTKGr8DpZu417tZLnBu5ywO8TIg3iCSLxJ0CyDA5SkapAkpa5tD3wW8L4ZRZ3nU5NspznumdYi9pG2q6PD1mCJECQCZmJN7Tcrm24n7qZxcXcYBB9YBFtDcFTZZFKjscHxBsWtM662WnQ4m0i7gOXX+688pY0iYOjswMaTr5aI/DcRZm7/AMpgmCJEfMBznxCfYwb0dw/HM0zD3j3Qj8WP6h6rjMRxhmYhhgScombTa6GdxTr9U3pNg9ZI7CvUBiHD1VLntvOQ+65B3EnHSfzmq3Y6pzRWmsR6lI65xkGC2BG/kqa+HflDo7rpAOxyxMeoXJfr36T4jY+KIw3H8QyMlQjKZAsQLtdoZGrGn/5Uekl7C/i4e5p15GrT/Ky62MAOnnslX4/Xe4ucQSTJ7rQPJrQAPJUniGb56bTPKyb8JMH4uBF+NCoOJHVQqUgflkdD91X+nKdaV/BVLVIKouzOEyRNwCAT4Egx6Kis+NDfedrfefKFFlB3MDzAV/6cuNy1WR0kmyuWrVAdepc5Zy7ZiCfMgD+FZhXag2DhBMA7zYnTTbwVlZjGTugsRiJdpA6IywRjwxYZpydoKxGHDbZgeoMx0VTaQkAn6BC0607K2J3Uior2JLdfJtUOGU4BuRzRBw7GgnKLCeaH4VjAYYbQLa3VnF6pDMo1cYXqx9NY98Uv5PLl6jybW/8Ahdh6oLMwsD91bmsAs7incYxgsJE+SKdU+QakmP7qyGTna/ahJY+Ny92y1SBSyKQpHkryptESosCi8qbEGMiVHC1WBogE3O0sHVxEBTa0F47aaZfALntNmAwXAD5vrHNOkuQTs62qROjSOYkOsDaNBeJ/4gz4JPHezZZmc7RN51MDf6pkkrfIVyQLQYv6i+ipq1crSAdRfeft4JJKIgNQbeYutDtMxy5SDH8J0kzAgStTdM7R7bofEjbbTkJSSRiKwKDETEG/5yVLnE21/jokktEEZpui803hocdDIBB1iJBjxGvRUEpJK7G7KsvBKnRDgTMEEWg3mZM7RA9VdTocvzz+xSSVyRS2TeIMctNI/PIKNakMwGeQdzIAvFxHSbJJI0K2VYjDlj3NJuCRvfqJvG9wNVKhvLo7piWh0nYX+Wf6tkkk0eYoD4kQlOEkk6EfBMWUQ6RLe94JJJruW0CX9LkN2VQ/s9SE7aG73taPGSkknyY1D9wY5OfHRRVq05gNzDmSRPhCGruBOgHqfWUklilJs1xjQqNAuNrDmbAeaJq0mgDLmPNxEA+CSSs2JREk3ZZwue0bAJv7brV4szubTIjqUyS04V+RIxZX+fFFXGmHIwHYifRHspaH06WSSWiC/Ml9EZ5yfpx+5cGgJVX8kkloKEr5KA1XMw5hJJBhlJo//9k='),
            createImage('https://s-media-cache-ak0.pinimg.com/736x/8c/5a/d3/8c5ad39a11271371bcded66acea484f3.jpg'),
            createImage('https://s-media-cache-ak0.pinimg.com/736x/f7/52/7f/f7527f43b8ec4658d88d73c98d78b86c.jpg'),
            createImage('https://lh3.googleusercontent.com/Cn9VGIctks_jhwdbcQANkUphinqdrJkLrZ-GAJOYJ1a57lzLaffy3fhWl2C4_aZXVQ=w300'),
        ],
        selected_index: -1,
        top: 0,
        left: 0,
        width: 50,
        height: 50,
        moveLeft: moveLeft(),
        moveRight: moveRight(),
        moveUp: moveUp(),
        moveDown: moveDown()
    }
    $scope.select_overlay = function(idx){
        if($scope.overlay.selected_index === idx){
            $scope.overlay.selected_index = -1;
        } else {
            $scope.overlay.selected_index = idx;
        }
    }
    $scope.$watch('overlay.selected_index', function(){
        drawImage();
    });
    $scope.$watch('overlay.top', function(){
        drawImage();
    });
    $scope.$watch('overlay.left', function(){
        drawImage();
    });

    $scope.submit = function(){
        console.log('submit');
        var data = canvas.toDataURL('image/jpeg');
        $http.post('/photos/edited', {data: data}).then(function(resp){
              $location.path('/');
        }, function(err){
            $scope.error = err;
        })
    }

    window.addEventListener('resize', init);

    init();
    
    function init(){
        canvas.width = $('#photo-canvas-container').width();
        // canvas.height =$(window).height() - 20;
        drawImage();
    }

    function drawImage(){
        console.log('drawImage');
        var img = new Image();
        img.addEventListener('load', function(){
            //canvas.width = img.width;
            //canvas.height = img.height;
            var ratio = canvas.width / img.width;
            var w = canvas.width;
            var h = img.height * ratio * 0.93;
            canvas.height = h;
            ctx.drawImage(img, 0, 0, w, h);
            if($scope.overlay.selected_index > -1 && $scope.overlay.selected_index < $scope.overlay.imgs.length){
                console.log('drawing overlay');
                var overlay_img = $scope.overlay.imgs[$scope.overlay.selected_index];
                ctx.drawImage(
                    overlay_img,
                    $scope.overlay.left, 
                    $scope.overlay.top, 
                    $scope.overlay.width, 
                    $scope.overlay.height);
            }
            ctx.font = '' + $scope.text1.fontSize + 'px serif';
            ctx.fillStyle = $scope.text1.color;
            ctx.fillText($scope.text1.contents, $scope.text1.left, $scope.text1.top);
            ctx.font = '' + $scope.text2.fontSize + 'px serif';
            ctx.fillStyle = $scope.text2.color;
            ctx.fillText($scope.text2.contents, $scope.text2.left, $scope.text2.top);
        }, false);
        img.src = '/photos/' + photo_id + '/image';        
    }

    function moveLeft(){
        return function(){
            this.left -= SPEED;
            if(this.left < 0){
                this.left = 0;
            }  
        }      
    }
    function moveRight(){
        return function(){
            this.left += SPEED;
            if(this.left > canvas.width){
                this.left = canvas.width;
            }  
        }      
    }
    function moveUp(){
        return function(){
            this.top -= SPEED;
            if(this.top < 0){
                this.top = 0;
            }
        }
    }
    function moveDown(){
        return function(){
            this.top += SPEED;
            if(this.top > canvas.height){
                obj.top = canvas.height;
            }
        }
    }
    function createImage(url){
        var img = new Image();
        img.src = url;
        return img;
    }


}]);


///////////////////////////////////////////////////////////////////////////////
/**
* WhilePressed directive
*/
angular.module('app').directive('whilePressed', function($parse, $interval){
    var TICK_LENGTH = 15;
    return {
        restrict: 'A',
        link: function(scope, elem, attrs){
            var action = $parse(attrs.whilePressed);
            var intervalPromise = null;
            function bindWhilePressed(){
                elem.on('mousedown', beginAction);
            }
            function bindEndAction(){
                elem.on('mouseup', endAction);
                elem.on('mouseleave', endAction);
            }
            function unbindEndAction(){
                elem.off('mouseup', endAction);
                elem.off('mouseleave', endAction);
            }
            function beginAction(e){
                e.preventDefault();
                tickAction();
                intervalPromise = $interval(tickAction, TICK_LENGTH);
                bindEndAction();
            }
            function endAction(){
                $interval.cancel(intervalPromise);
                unbindEndAction();
            }
            function tickAction(){
                action(scope);
            }
            bindWhilePressed();
        }
    }

});


/**
* File Upload Directive
*/
angular.module('app').directive('fileUpload', function(){
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function link(scope, element, attrs, ngModel) {
            if(!ngModel) return;
            element.on('blur keyup change', function() {
                scope.$evalAsync(read);
            });
            read();

            function read() {
                var file = element[0].files[0];
                if(!file) return;
                var reader = new FileReader();
                reader.onload = (function(f){
                    return function(e){
                        ngModel.$setViewValue({string: e.target.result});
                        element[0].value = null;
                    }
                })(file);
                reader.readAsDataURL(file);
            }
        }
    };
});


