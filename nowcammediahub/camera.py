#!/env/bin/python

import telnetlib
import sys


def run_command(host, cmd):
    try:
        t = telnetlib.Telnet(host, timeout=5)
        t.read_until('#', 5)
        t.write('{}\n'.format(cmd))
        res = t.read_until('#', 5)
        t.write('exit\n')
        t.read_all()
        return res
    finally:
        if t: t.close()


def check_wifisd(host):
    # try:
    #     t = telnetlib.Telnet(host, timeout=5)
    #     t.read_until('#', 5)
    #     # t.write('ps | grep udpsvd | grep -v "grep udpsvd"\n')
    #     # t.write('ps | grep "udpsvd 0 5010" | grep -v "grep udpsvd"\n')
    #     t.write('ps\n')
    #     res = t.read_until('#', 5)
    #     # print 'result: ^^^{}^^^'.format(res)
    #     t.write('exit\n')
    #     t.read_all()
    #     return res
    # finally:
    #     if t: t.close()
    return run_command(host, 'ps')


def launch_broacast_listener(host):
    # try:
    #     t = telnetlib.Telnet(host, timeout=5)
    #     t.read_until('#', 5)
    #     t.write('udpsvd 0 5010 /opt/bin/busybox tee /nowcam_media_hub > /dev/null &\n')
    #     res = t.read_until('#', 5)
    #     t.write('exit\n')
    #     t.read_all()
    #     return res
    # finally:
    #     if t: t.close()
    return run_command(host, 'udpsvd 0 5010 /opt/bin/busybox tee /nowcam_media_hub > /dev/null &')


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Usage: {} <ip address of wifi sd card>'.format(sys.argv[0])
        sys.exit(0)
    host = sys.argv[1]
    print 'connecting to {}'.format(host)
    print check_wifisd(host)
