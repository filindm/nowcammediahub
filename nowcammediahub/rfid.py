# coding=UTF-8
from flask import jsonify, request, abort
import requests
import traceback
from nowcammediahub import app, db, get_default_event, AUTH_HDRS, VERIFY_SSL
import cors


@app.route('/rfid.html')
def rfid():
    return app.send_static_file('rfid.html')


@app.route('/api/rfid', methods=['POST'])
@cors.crossdomain(origin='*')
def set_current_rfid():
    if not request.json:
        abort(415, 'JSON required, but not found')
    rfid = request.json['rfid']
    db.rfid.replace_one({'_id': 1}, {'rfid': rfid}, upsert=True)
    return 'ok'


@app.route('/api/rfid', methods=['GET'])
@cors.crossdomain(origin='*')
def get_current_rfid():
    try:
        rfid = db.rfid.find_one_and_delete({'_id': 1})['rfid']
        return jsonify({'rfid': rfid})
    except:
        return jsonify({'rfid': ''})


@app.route('/api/rfid/<rfid>', methods=['GET'])
@cors.crossdomain(origin='*')
def get_user_by_rfid(rfid):
    try:
        res = requests.get(app.config['NOWCAM_CENTRAL_URL'] + '/users/rfid/' + rfid, headers=AUTH_HDRS, verify=VERIFY_SSL)
        res.raise_for_status()
        res = res.json()
        return jsonify({
            'first_name': res['details']['first_name'], 
            'last_name': res['details']['last_name']
        })
    except:
        traceback.print_exc()
        return jsonify({})


@app.route('/api/user_data', methods=['POST'])
def submit_user_data():
    data = {
        'first_name': request.json['firstName'],
        'last_name': request.json['lastName'],
        'email': request.json['email'],
        'fb_access_token': request.json['accessToken'],
        'event_id': get_default_event()['_id']
    }
    res = requests.post(app.config['NOWCAM_CENTRAL_URL'] + '/rfid/user_data', data=json.dumps(data),
        headers=AUTH_HDRS, verify=VERIFY_SSL)
    res.raise_for_status()
    db.rfid.update_one({'_id': 1}, {'$set': {'rfid': ''}}, upsert=True)
    return 'ok'
