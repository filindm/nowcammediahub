import platform


if platform.system() == 'Windows':
    from printing_win import *
else:
    from printing_nix import *

