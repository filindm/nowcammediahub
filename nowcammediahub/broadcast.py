
import time
from socket import *

def main():
    s = socket(AF_INET, SOCK_DGRAM)
    # s.bind(('', 0))
    s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
    s.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)

    while True:
        try:
            host = gethostbyname(gethostname())
            data = 'NowcamMediaHub:{}'.format(host)
            s.sendto(data, ('255.255.255.255', 5010))
        except:
            pass
        time.sleep(3)

if __name__ == '__main__':
    main()
