import win32print
import win32ui
from PIL import Image, ImageWin, ImageDraw, ImageFont
import logging


logger = logging.getLogger(__name__)

def print_image(name, file, printer_name):
    #
    # Constants for GetDeviceCaps
    #
    #
    # HORZRES / VERTRES = printable area
    #
    HORZRES = 8
    VERTRES = 10
    #
    # LOGPIXELS = dots per inch
    #
    LOGPIXELSX = 88
    LOGPIXELSY = 90
    #
    # PHYSICALWIDTH/HEIGHT = total area
    #
    PHYSICALWIDTH = 110
    PHYSICALHEIGHT = 111
    #
    # PHYSICALOFFSETX/Y = left / top margin
    #
    PHYSICALOFFSETX = 112
    PHYSICALOFFSETY = 113

    printer_name = printer_name or win32print.GetDefaultPrinter()

    #
    # You can only write a Device-independent bitmap
    #  directly to a Windows device context; therefore
    #  we need (for ease) to use the Python Imaging
    #  Library to manipulate the image.
    #
    # Create a device context from a named printer
    #  and assess the printable size of the paper.
    #
    hDC = win32ui.CreateDC()
    try:
        hDC.CreatePrinterDC(printer_name)
        printable_area = hDC.GetDeviceCaps (HORZRES), hDC.GetDeviceCaps (VERTRES)
        printer_size = hDC.GetDeviceCaps (PHYSICALWIDTH), hDC.GetDeviceCaps (PHYSICALHEIGHT)
        printer_margins = hDC.GetDeviceCaps (PHYSICALOFFSETX), hDC.GetDeviceCaps (PHYSICALOFFSETY)

        logger.debug('printable area: {}'.format(printable_area))
        logger.debug('printer size: {}'.format(printer_size))
        logger.debug('printer margins: {}'.format(printer_margins))
        #
        # Open the image, rotate it if it's wider than
        #  it is high, and work out how much to multiply
        #  each pixel by to get it as big as possible on
        #  the page without distorting.
        #
        if isinstance(file, Image.Image):
            bmp = file
        else:
            bmp = Image.open(fp=file)
        if bmp.size[0] > bmp.size[1] and printable_area[1] > printable_area[0]:
            bmp = bmp.transpose(Image.ROTATE_90)
        if bmp.size[1] > bmp.size[0] and printable_area[0] > printable_area[1]:
            bmp = bmp.transpose(Image.ROTATE_90)

        ratios = [1.0 * printable_area[0] / bmp.size[0], 1.0 * printable_area[1] / bmp.size[1]]
        scale = max (ratios)
        logger.debug('scale: {}'.format(scale))

        #
        # Start the print job, and draw the bitmap to
        #  the printer device at the scaled size.
        #
        hDC.StartDoc(name)
        hDC.StartPage()

        dib = ImageWin.Dib(bmp)
        scaled_width, scaled_height = [int (scale * i) for i in bmp.size]
        logger.debug('scaled (w, h): {}, {}'.format(scaled_width, scaled_height))

        offset_x = int((scaled_width - printable_area[0]) / 2)
        offset_y = int((scaled_height - printable_area[1]) / 2)

        x1 = int ((scaled_width - printable_area[0]) / 2)
        y1 = int ((scaled_height - printable_area[1]) / 2)
        x2 = scaled_width - x1
        y2 = scaled_height - y1
        
        dst = (0, 0, printable_area[0], printable_area[1])
        src = (offset_x, offset_y, scaled_width - offset_x, scaled_height - offset_y)
        logger.debug('draw, src: {}, dst: {}'.format(src, dst))
        dib.draw(hDC.GetHandleOutput(), dst)

        hDC.EndPage()
        hDC.EndDoc()
    
    finally:
        hDC.DeleteDC()


def print_ticket(code, img_file, ticket_img_file, printer_name, text_box):
    try:
        img = Image.open(img_file).convert(mode='L')
        size = (900, int(img.height * (900 / float(img.width))))
        img = img.resize(size)
        ticket_img = Image.open(ticket_img_file).convert(mode='L')
        size = (900, int(ticket_img.height * (900 / float(ticket_img.width))))
        ticket_img = ticket_img.resize(size)
        size = (900, img.height + ticket_img.height)
        res_img = Image.new(mode='L', size=size)
        res_img.paste(img, (0, 0))
        res_img.paste(ticket_img, (0, img.height))
        draw = ImageDraw.Draw(res_img)
        # text_box = {
        #     'x1': res_img.width / 2 - 100,
        #     'y1': res_img.height / 2 - 30,
        #     'x2': res_img.width / 2 + 100,
        #     'y2': res_img.height / 2 + 30,
        #     'fill': 'white'
        # }
        logger.debug('orig text_box: ' + str(text_box))
        logger.debug('res_img.size: ' + str(res_img.size))
        text_box = {
            'x1': res_img.width * text_box['left'] / 100,
            'y1': res_img.height * text_box['top'] / 100,
            'x2': res_img.width * text_box['left'] / 100 + res_img.width * text_box['width'] / 100,
            'y2': res_img.height * text_box['top'] / 100 + res_img.height * text_box['height'] / 100,
            'fill': 'white'
        }
        logger.debug('text_box: ' + str(text_box))
        draw.rectangle([text_box['x1'], text_box['y1'], text_box['x2'], text_box['y2']], fill='white')
        font = ImageFont.truetype("arial.ttf", 72)
        draw.text((text_box['x1'] + 8, text_box['y1'] + 10), code, font=font)
        # print_image(code, res_img, printer_name)
        _do_print_ticket(code, res_img, printer_name)
        res_img.save('c:\\nowcammediahub\\ticket.jpg')

    except Exception as ex:
        logger.error(ex)


def _do_print_ticket(name, file, printer_name):
    #
    # Constants for GetDeviceCaps
    #
    #
    # HORZRES / VERTRES = printable area
    #
    HORZRES = 8
    VERTRES = 10
    #
    # LOGPIXELS = dots per inch
    #
    LOGPIXELSX = 88
    LOGPIXELSY = 90
    #
    # PHYSICALWIDTH/HEIGHT = total area
    #
    PHYSICALWIDTH = 110
    PHYSICALHEIGHT = 111
    #
    # PHYSICALOFFSETX/Y = left / top margin
    #
    PHYSICALOFFSETX = 112
    PHYSICALOFFSETY = 113

    #
    # You can only write a Device-independent bitmap
    #  directly to a Windows device context; therefore
    #  we need (for ease) to use the Python Imaging
    #  Library to manipulate the image.
    #
    # Create a device context from a named printer
    #  and assess the printable size of the paper.
    #
    hDC = win32ui.CreateDC()
    try:
        hDC.CreatePrinterDC(printer_name)
        printable_area = hDC.GetDeviceCaps (HORZRES), hDC.GetDeviceCaps (VERTRES)
        printer_size = hDC.GetDeviceCaps (PHYSICALWIDTH), hDC.GetDeviceCaps (PHYSICALHEIGHT)
        printer_margins = hDC.GetDeviceCaps (PHYSICALOFFSETX), hDC.GetDeviceCaps (PHYSICALOFFSETY)

        logger.debug('printable area: {}'.format(printable_area))
        logger.debug('printer size: {}'.format(printer_size))
        logger.debug('printer margins: {}'.format(printer_margins))
        #
        # Open the image, rotate it if it's wider than
        #  it is high, and work out how much to multiply
        #  each pixel by to get it as big as possible on
        #  the page without distorting.
        #
        if isinstance(file, Image.Image):
            bmp = file
        else:
            bmp = Image.open(fp=file)
        # if bmp.size[0] > bmp.size[1] and printable_area[1] > printable_area[0]:
        #     bmp = bmp.transpose(Image.ROTATE_90)
        # if bmp.size[1] > bmp.size[0] and printable_area[0] > printable_area[1]:
        #     bmp = bmp.transpose(Image.ROTATE_90)

        scale = 1.0 * printable_area[0] / bmp.size[0] # only scale width
        logger.debug('scale: {}'.format(scale))

        #
        # Start the print job, and draw the bitmap to
        #  the printer device at the scaled size.
        #
        hDC.StartDoc(name)
        hDC.StartPage()

        dib = ImageWin.Dib(bmp)
        scaled_width, scaled_height = [int (scale * i) for i in bmp.size]
        logger.debug('orig size: ' + str(bmp.size))
        logger.debug('scaled size (w, h): {}, {}'.format(scaled_width, scaled_height))

        offset_x = int((scaled_width - printable_area[0]) / 2)
        offset_y = int((scaled_height - printable_area[1]) / 2)

        x1 = int ((scaled_width - printable_area[0]) / 2)
        y1 = int ((scaled_height - printable_area[1]) / 2)
        x2 = scaled_width - x1
        y2 = scaled_height - y1
        
        dst = (0, 0, printable_area[0], printable_area[1])
        src = (offset_x, offset_y, scaled_width - offset_x, scaled_height - offset_y)
        logger.debug('draw, src: {}, dst: {}'.format(src, dst))
        dib.draw(hDC.GetHandleOutput(), dst)

        hDC.EndPage()
        hDC.EndDoc()

    except Exception as ex:
        logger.error(ex)
    
    finally:
        hDC.DeleteDC()


def enum_printers():
    printers = [name for _,_,name,_ in win32print.EnumPrinters(win32print.PRINTER_ENUM_LOCAL)]
    printers.extend(['<No auto-print>'])
    return printers


if __name__ == '__main__':
    print_image('test.jpg', open('test.jpg', 'rb'))

