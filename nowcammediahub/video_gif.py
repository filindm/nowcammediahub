import time
import os
import os.path
import subprocess
try:
    from PIL import Image
except ImportError:
    from pil import Image
from io import BytesIO
from pymongo import MongoClient
import gridfs


if os.name == 'posix':
    IMAGEMAGICK_CONVERT = ['convert']
else:
    IMAGEMAGICK_CONVERT = ['magick', 'convert']


PAUSE = 1.0
os.stat_float_times(True)


import config
db = MongoClient(connect=False)[config.MONGO_DBNAME]
fs = gridfs.GridFS(db)


def main():
    if os.name == 'posix':
        dirname = os.path.expanduser('~/NowcamVideoGif/gif')
        dst_dirname = os.path.expanduser('~/NowcamTetheredCamera/img_processing')
    else:
        dirname = 'C:\\NowcamVideoGif\\gif'
        dst_dirname = 'C:\\NowcamTetheredCamera\\img_processing'
    try:
        while True:
            file_groups = split_files(dirname, collect_files(dirname))
            for files in file_groups:
                gif = process_files(dirname, *files)
                for f in files:
                    os.unlink(os.path.join(dirname, f))
                os.rename(os.path.join(dirname, gif), os.path.join(dst_dirname, gif))
            time.sleep(PAUSE)
    except KeyboardInterrupt:
        pass


def ctime(path): 
    return os.stat(path).st_ctime


def collect_files(dirpath):
    def path(f): return os.path.join(dirpath, f)
    l = [x for x in os.listdir(dirpath) if x.lower().endswith('.jpg') and os.path.isfile(path(x))]
    return sorted(l, cmp=lambda x,y: cmp(ctime(path(x)), ctime(path(y))))


def split_files(dirpath, files):
    now = time.time()
    res = []
    cur = []
    def path(f): return os.path.join(dirpath, f)
    for i in range(len(files)):
        f = files[i]
        cur.append(f)
        if i + 1 < len(files):
            if ctime(path(files[i+1])) - ctime(path(f)) >= PAUSE:
                res.append(cur)
                cur = []
        else:
            if now - ctime(path(f)) >= PAUSE:
                res.append(cur)
    return res


def process_files(dirpath, *filenames):
    if not len(filenames):
        return
    dst_filename = filenames[0][0:-3] + 'gif'
    dst_path = os.path.join(dirpath, dst_filename)
    size = (900, 600)
    images = [Image.open(os.path.join(dirpath, fn)).resize(size, Image.LANCZOS) for fn in filenames]
    try:
        overlay = get_overlay().convert('RGBA').resize(size)
        images = map(lambda x: Image.alpha_composite(x.convert('RGBA'), overlay), images)
    except Exception as ex:
        print ex
    for img, fname in zip(images, filenames):
        img.save(os.path.join(dirpath, fname))
    cmd = []
    cmd.extend(IMAGEMAGICK_CONVERT)
    cmd.extend(['-resize', '900x600', '-loop', '0', '-delay', str(get_delay())])
    cmd.extend([os.path.join(dirpath, fn) for fn in filenames])
    cmd.append(dst_path)
    subprocess.check_call(cmd) 
    return dst_filename

#
# ffmpeg -i animated.gif -movflags faststart -pix_fmt yuv420p -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" video.mp4
#


def get_default_event():
    try:
        return db.settings.find_one({'_id': 1}).get('default_event')
    except:
        return ''


def get_overlay():
    overlay = fs.get(get_default_event()['overlay'])
    return Image.open(overlay)


def get_delay():
    DEFAULT_FPS = 4
    settings = db.settings.find_one({'_id': 1}).get('video_gifs_settings')
    if settings is not None:
        fps = int(settings.get('frames_per_second', DEFAULT_FPS))
    else:
        fps = DEFAULT_FPS
    return 100 / fps


if __name__ == '__main__':
    main()
