
DEBUG = False

SERVER_HOST = '0.0.0.0'
SERVER_PORT = 5010

NOWCAM_CENTRAL_URL = 'https://nowcam.nowmarketing.com.au'


### PyMongo
MONGO_URL = 'localhost'
MONGO_DBNAME = 'nowcam_mediahub'

### Logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '%(asctime)s - %(levelname)s - %(filename)s:%(lineno)d - %(message)s'
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'simple',
            'stream': 'ext://sys.stdout'
        },
        'file': {
            'class' : 'logging.handlers.RotatingFileHandler',
            'level': 'DEBUG',
            'formatter': 'simple',
            'filename': 'NowcamMediaHub.log',
            'maxBytes': 1024 * 1024,
            'backupCount': 3
        }
    },
    'loggers': {
        'root': {
            'level': 'DEBUG',
            'handlers': ['console', 'file']
        },
        'nowcammediahub': {
            'level': 'DEBUG',
            'handlers': ['console', 'file']
        },
    }
}
