#!/bin/bash

`dirname $0`/clean.sh
`dirname $0`/build.sh
(cd dist && virtualenv env && env/bin/pip install nowcammediahub-*.tar.gz)
dist/env/bin/nowcammediahub_debug

