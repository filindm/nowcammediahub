import requests
import StringIO
import argparse
import time
import os


AUTH_HDRS = {'Authorization': 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZGVudGl0eSI6IjU3MmNhMzNhM2Q2ZWU5MTc5MGIxZTRlZiIsImlhdCI6MTQ2NTI2MDcxMSwibmJmIjoxNDY1MjYwNzExLCJleHAiOjE2MjI5NDA3MTF9.S5Zm2T0EfANXnC3_MfizrtYIZIWMDQvmmwtAkexwn44'}
# localhost:5000:
# AUTH_HDRS = {'Authorization': 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZGVudGl0eSI6IjU3NGZkOGQ5OGE4NDhhZjc1Y2I0MTc5OSIsImlhdCI6MTQ2NTI2MDAxNCwibmJmIjoxNDY1MjYwMDE0LCJleHAiOjE0NjUyNjM2MTR9.EiiPifEJnlZDL_b9anwKz_c8Atz9WI_ryNZrwCxFBFQ'}


def create_remote_target(host, unit_id):
    def upload_photo(name, src):
        url = '{}/photos'.format(host)
        data = {
            'title': name,
            'unit_id': unit_id,
            'lastModifiedDate': int(time.time())
        }
        files = {
            'file': src
        }
        print 'uploading to {}'.format(url)
        r = requests.post(url, data=data, files=files, verify=False, headers=AUTH_HDRS)
        print r.text
        r.raise_for_status()
    return upload_photo


def create_local_target(path):
    def save_photo(name, src):
        with open(os.path.join(path, name), 'wb') as dst:
            while True:
                buf = src.read(8 *1024)
                if not buf: 
                    break
                dst.write(buf)
    return save_photo


def upload_photos(photos_num, target):
    r = requests.get(
        'https://api.flickr.com/services/rest/',
        params={
            'format': 'json',
            'nojsoncallback': 1,
            'method': 'flickr.interestingness.getList',
            'api_key': '7617adae70159d09ba78cfec73c13be3',
            'per_page': photos_num,
            'page': 1
        })
    r = r.json()
    if r['stat'] != 'ok':
        raise Exception('Flickr returned error: %s' % r['message'])

    i = 0
    for p in r['photos']['photo']:
        i = i + 1
        filename = '{}_{}_b.jpg'.format(p['id'], p['secret'])
        url = 'http://farm%(farm_id)s.staticflickr.com/%(server_id)s/%(filename)s' % {
            'farm_id': p['farm'],
            'server_id': p['server'],
            'filename': filename
        }
        # print p
        # print 'Title: %s' % title
        # print 'URL: %s' % url
        # print
        try:
            print 'downloading {}'.format(url)
            f = StringIO.StringIO(requests.get(url).content)
            # upload_photo(host=host, name=p['title'], file=f, unit_id=unit_id)
            target(name=filename, src=f)
        # except Exception as ex:
        finally:
            f.close()
            # print ex
        time.sleep(3)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', required=False, metavar='host', default='http://localhost:5010')
    parser.add_argument('--unit', required=False, metavar='unit_id', default="test")
    # parser.add_argument('--unit', required=True, metavar='unit_id')
    parser.add_argument('--path', required=False, metavar='path')
    parser.add_argument('--photos-number', required=False, metavar='photos_number', default=30)
    # parser.add_argument('--ssl', dest='ssl', action='store_true')
    # parser.add_argument('--no-ssl', dest='ssl', action='store_false')
    # parser.set_defaults(ssl=False)
    args = parser.parse_args()
    print '%r' % args
    if args.path:
        target = create_local_target(args.path)
    else:
        target = create_remote_target(args.host, args.unit)
    # upload_photos(host=args.host, unit_id=args.unit, photos_num=args.photos_number)
    upload_photos(photos_num=args.photos_number, target=target)


if __name__ == '__main__':
    main()







