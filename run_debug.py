#!env/bin/python
import subprocess
import signal
import os

os.environ['NOWCAM_TOKEN'] = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZGVudGl0eSI6IjU3MmNhMzNhM2Q2ZWU5MTc5MGIxZTRlZiIsImlhdCI6MTQ2NTI2MDcxMSwibmJmIjoxNDY1MjYwNzExLCJleHAiOjE2MjI5NDA3MTF9.S5Zm2T0EfANXnC3_MfizrtYIZIWMDQvmmwtAkexwn44'
os.environ['NOWCAM_CENTRAL_URL'] = 'http://localhost:5000'
from nowcammediahub import run_debug

p = subprocess.Popen('./watch')
run_debug()
p.send_signal(signal.SIGINT)
p.wait()

