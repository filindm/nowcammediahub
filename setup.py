from setuptools import setup
import sys
import os

VERSION = '1.2.2'

dependencies = [
    'CherryPy==5.0.1',
    'Flask==0.10.1',
    'Flask-PyMongo==0.4.0',
    'Pillow==3.1.0',
    'arrow==0.7.0',
    'pymongo==3.2',
    'requests==2.9.1'
]
if 'win32' == sys.platform:
    dependencies += ['pypiwin32==219']

def package_files(dir):
    paths = []
    for (path, _, filenames) in os.walk(dir):
        for filename in filenames:
            paths.append(os.path.join(path.replace('nowcammediahub/', ''), filename))
    return paths

extra_files = package_files('nowcammediahub/static')
extra_files += package_files('nowcammediahub/templates')
# print extra_files

setup(
    name='nowcammediahub',
    version=VERSION,
    install_requires=dependencies,
    entry_points={
        'console_scripts': [
            'nowcammediahub=nowcammediahub:run',
            'nowcammediahub_debug=nowcammediahub:run_debug',
            'nowcammediahub_broadcast=nowcammediahub:broadcast_main',
            'nowcammediahub_tethered_camera=nowcammediahub:tethered_camera_main',
            'nowcammediahub_video_gif=nowcammediahub:video_gif_main',
        ],
    },
    packages=['nowcammediahub'],
    package_data={'': extra_files}
)
