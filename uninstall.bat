setup\win\nssm stop "NowcamMediaHub Broadcast"
setup\win\nssm remove "NowcamMediaHub Broadcast" confirm

setup\win\nssm stop NowcamMediaHub
setup\win\nssm remove NowcamMediaHub confirm

setup\win\nssm stop "NowcamMediaHub Task Queue"
setup\win\nssm remove "NowcamMediaHub Task Queue" confirm

setup\win\nssm stop "NowcamMediaHub Message Broker"
setup\win\nssm remove "NowcamMediaHub Message Broker" confirm
"C:\Program Files\RabbitMQ Server\uninstall.exe" /S 
"C:\Program Files\erl7.2.1\Uninstall.exe" /S 

msiexec /x setup\win\python-2.7.11.msi /quiet /qn /norestart

setup\win\nssm stop MongoDB
setup\win\nssm remove MongoDB confirm
msiexec.exe /q /x setup\win\mongodb-win32-x86_64-2008plus-ssl-3.2.0-signed.msi

