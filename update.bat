@echo off

set nssm=c:\programdata\chocolatey\bin\nssm.exe

%nssm% stop "NowcamMediaHub"
%nssm% stop "NowcamMediaHub Broadcast"
%nssm% stop "NowcamTetheredCamera"
%nssm% stop "NowcamVideoGif"

set PATH="%PATH%;C:\Program Files\Git\cmd"
del "%TMP%\NowcamMediaHub.zip"
git archive --format=zip --prefix=NowcamMediaHub/ --remote=git@bitbucket.org:filindm/nowcammediahub.git HEAD > "%TMP%\NowcamMediaHub.zip"
c:\tools\python2\scripts\pip install --upgrade "%TMP%\NowcamMediaHub.zip"
del "%TMP%\NowcamMediaHub.zip"

%nssm% start "NowcamMediaHub"
%nssm% start "NowcamMediaHub Broadcast"
%nssm% start "NowcamTetheredCamera"
%nssm% start "NowcamVideoGif"
